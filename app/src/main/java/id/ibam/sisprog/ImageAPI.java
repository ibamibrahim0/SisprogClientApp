package id.ibam.sisprog;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Ibam on 12/22/2017.
 */

public interface ImageAPI {
    @Multipart
    @POST("/api")
    Call<ImageResponse> postImage(@Part MultipartBody.Part upload);
}
