package id.ibam.sisprog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ibam on 12/22/2017.
 */

public class ImageData {
    @SerializedName("img_name")
    @Expose
    private String imgName;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("img_view")
    @Expose
    private String imgView;
    @SerializedName("img_width")
    @Expose
    private String imgWidth;
    @SerializedName("img_height")
    @Expose
    private String imgHeight;
    @SerializedName("img_attr")
    @Expose
    private String imgAttr;
    @SerializedName("img_size")
    @Expose
    private String imgSize;
    @SerializedName("img_bytes")
    @Expose
    private Integer imgBytes;
    @SerializedName("thumb_url")
    @Expose
    private String thumbUrl;
    @SerializedName("thumb_width")
    @Expose
    private Integer thumbWidth;
    @SerializedName("thumb_height")
    @Expose
    private Integer thumbHeight;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("resized")
    @Expose
    private String resized;
    @SerializedName("delete_key")
    @Expose
    private String deleteKey;

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgView() {
        return imgView;
    }

    public void setImgView(String imgView) {
        this.imgView = imgView;
    }

    public String getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(String imgWidth) {
        this.imgWidth = imgWidth;
    }

    public String getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(String imgHeight) {
        this.imgHeight = imgHeight;
    }

    public String getImgAttr() {
        return imgAttr;
    }

    public void setImgAttr(String imgAttr) {
        this.imgAttr = imgAttr;
    }

    public String getImgSize() {
        return imgSize;
    }

    public void setImgSize(String imgSize) {
        this.imgSize = imgSize;
    }

    public Integer getImgBytes() {
        return imgBytes;
    }

    public void setImgBytes(Integer imgBytes) {
        this.imgBytes = imgBytes;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public Integer getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(Integer thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public Integer getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(Integer thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getResized() {
        return resized;
    }

    public void setResized(String resized) {
        this.resized = resized;
    }

    public String getDeleteKey() {
        return deleteKey;
    }

    public void setDeleteKey(String deleteKey) {
        this.deleteKey = deleteKey;
    }

}
