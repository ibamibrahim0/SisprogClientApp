package id.ibam.sisprog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ibam on 12/22/2017.
 */

public class ImageResponse {
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_txt")
    @Expose
    private String statusTxt;
    @SerializedName("data")
    @Expose
    private ImageData data;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusTxt() {
        return statusTxt;
    }

    public void setStatusTxt(String statusTxt) {
        this.statusTxt = statusTxt;
    }

    public ImageData getData() {
        return data;
    }

    public void setData(ImageData data) {
        this.data = data;
    }
}
