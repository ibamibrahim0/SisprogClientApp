package id.ibam.sisprog;

/**
 * Created by Ibam on 12/22/2017.
 */

public class RaspData {
    private boolean status;
    private String image;

    public static RaspData createData(boolean status, String image) {
        RaspData data = new RaspData();
        data.setImage(image);
        data.setStatus(status);
        return data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
