package id.ibam.sisprog;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

/**
 * Created by Ibam on 12/22/2017.
 */

public interface RaspInterface {
    @PUT("/bins/rr49j")
    Call<ResponseBody> postToRasp(@Body RaspData data);
}
